//
//  NSTextFieldLogger.m
//  Sonoff Devices DIY Tool
//
//  Created by Ed The Head on 2020-09-29.
//

#import "NSTextViewLogger.h"

@interface NSTextViewLogger ()

@property (nonatomic, strong) NSMutableArray *logMsgCache;

@end

@implementation NSTextViewLogger

@synthesize textView;
@synthesize autoScrollsToBottom;
@synthesize logFormatter;
@synthesize logMsgCache;
@synthesize font;

- (id)init {
  if (self = [super init]) {
      autoScrollsToBottom = YES;
      font = [NSFont fontWithName:@"Andale Mono" size:12];
  }
  return self;
}

#pragma mark - Private Stuff

- (void)appendTextViewString:(NSString *)string {
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        //[self->textView setFont:self->font];
        
        NSAttributedString *attributedString =[[NSAttributedString alloc] initWithString:string];
        [[self->textView textStorage] appendAttributedString:attributedString];
        if (self.autoScrollsToBottom) {
            [self->textView scrollRangeToVisible:NSMakeRange([[self->textView textStorage] length], 0)];
        }
    });
}

#pragma mark - DDLogger

- (void)logMessage:(nonnull DDLogMessage *)logMessage {
    NSString *logMsg = [logMessage message];
    
    if(logFormatter){
        logMsg = [logFormatter formatLogMessage:logMessage];
    }
    
    if (logMsg) {
        /* if textView is available, write to it,
        otherwise cache it */
        if (self.textView) {
            [self appendTextViewString:[NSString stringWithFormat:@"\n%@", logMsg]];
        } else {
            if (!self.logMsgCache) {
                self.logMsgCache = [NSMutableArray array];
            }
            
            [self.logMsgCache addObject:logMsg];
        }
    }
}

- (nullable NSString *)formatLogMessage:(nonnull DDLogMessage *)logMessage {
    NSString *logLevel;
    switch (logMessage->_flag) {
        case DDLogFlagError    : logLevel = @"E"; break;
        case DDLogFlagWarning  : logLevel = @"W"; break;
        case DDLogFlagInfo     : logLevel = @"I"; break;
        case DDLogFlagDebug    : logLevel = @"D"; break;
        default                : logLevel = @"V"; break;
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MMM-yyyy HH:mm:ss"];
    NSString *dateAndTime = [formatter stringFromDate:(logMessage->_timestamp)];
    NSString *logMsg = logMessage->_message;

    return [NSString stringWithFormat:@"[%@] %@", dateAndTime, logMsg];
}

@end
