//
//  ViewController.m
//  Sonoff Devices DIY Tool
//
//  Created by Ed The Head on 2020-09-27.
//

#import "ViewController.h"

#import <netinet/in.h>
#import <sys/socket.h>
#include <arpa/inet.h>

#import <CoreWLAN/CoreWLAN.h>

#import "NSTextViewLogger.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        
    _font = [NSFont fontWithName:@"Andale Mono" size:10];
    [[[self logTextView] textStorage] setFont:_font];
    
    NSTextViewLogger *textViewLogger = [[NSTextViewLogger alloc] init];
    [textViewLogger setLogFormatter:textViewLogger];
    [textViewLogger setTextView: _logTextView];
    [DDLog addLogger:textViewLogger];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(somethingHappens:) name:@"notificationName" object:nil];
    
    _sonoffAPI = [[SonoffAPI alloc] init];
    _services = [NSMutableArray arrayWithCapacity:0];
    
    _netServiceBrowser = [[NSNetServiceBrowser alloc] init];
    [_netServiceBrowser scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [_netServiceBrowser setDelegate:self];
    [_netServiceBrowser includesPeerToPeer];
    [_netServiceBrowser searchForServicesOfType:@"_ewelink._tcp." inDomain:@"local"];
    
    
    if ([_webServerCheckBox state] == true)
    {
        [self startWebServer:nil];
    }
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

#pragma mark Actions


- (IBAction)currentStateAction:(id)sender
{
    if (_sonoffAPI==nil)
    {
        DDLogError(@"API object not created");
        return;
    }
    NSSwitch *uiSwitch = (NSSwitch*)sender;
    bool state = [uiSwitch state];

    Device *d = (Device*)[[_arrayController arrangedObjects] objectAtIndex: [_tableView selectedRow]];
    [_sonoffAPI set_switch_state:d state:state];
    
}

-(IBAction)startupStateAction:(id)sender
{
    NSButton * button = sender;
    Device *d = (Device*)[[_arrayController arrangedObjects] objectAtIndex: [_tableView selectedRow]];

    if ([button tag]==0)
    {
        [_sonoffAPI set_powerup_state:d state:@"off"];
        [d setStartupState:@"off"];
    } else
    if ([button tag]==1)
    {
        [_sonoffAPI set_powerup_state:d state:@"on"];
        [d setStartupState:@"on"];
    } else
    if ([button tag]==2)
    {
        [_sonoffAPI set_powerup_state:d state:@"stay"];
        [d setStartupState:@"stay"];
    }
}

- (IBAction)otaUnlockStateAction:(id)sender {
    NSSwitch *uiSwitch = (NSSwitch*)sender;
    bool state = [uiSwitch state];
    
    Device *d = (Device*)[[_arrayController arrangedObjects] objectAtIndex:[_tableView selectedRow]];
    
    [_sonoffAPI set_ota_unlock:d state:state];
}

- (IBAction)firmwareFileChooserAction:(id)sender {
    NSOpenPanel *openDialog = [NSOpenPanel openPanel];
    [openDialog setCanChooseFiles:TRUE];
    [openDialog setAllowsMultipleSelection:FALSE];
    [openDialog setCanChooseDirectories:FALSE];
    if ( [openDialog runModal] == NSModalResponseOK )
    {
        NSURL *url = [openDialog URL];
        NSData *digest = [self sha256:url];
        [_firmwareFileName setStringValue:[url path]];
        [_firmwareFileName setToolTip:[NSString stringWithFormat:@"SHA256\n%@",[self hexStringForData:digest]]];
        unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:[url path] error:nil] fileSize];
        [_downloadProgressBar setDoubleValue:0];
        [_downloadProgressBar setMinValue:0];
        [_downloadProgressBar setMaxValue:fileSize];
    }
}

- (IBAction)flashButtonAction:(id)sender {   
    NSError *error;
    NSRegularExpression *regex = [[NSRegularExpression alloc] initWithPattern:@"^(?:http|https)?(?:\\:\\/\\/)?([\\w\\d\\s\\.]*)?(?:\\:)?(\\d*)" options:0 error:&error];
    NSString *webAddressTextField = [_webServerAddress stringValue];
    NSString *webAddress;
    NSString *WebAddressPort;
    NSArray* matches = [regex matchesInString:webAddressTextField options:0 range:NSMakeRange(0, [webAddressTextField length])];
    for (NSTextCheckingResult* match in matches) {
        //NSString* matchText = [webAddressTextField substringWithRange:[match range]];
        NSRange group1 = [match rangeAtIndex:1];
        NSRange group2 = [match rangeAtIndex:2];
        webAddress = [webAddressTextField substringWithRange:group1];
        WebAddressPort = [webAddressTextField substringWithRange:group2];
    }
    Device *d = (Device*)[[_arrayController arrangedObjects] objectAtIndex:[_tableView selectedRow]];
    
    NSString *fileName = [_firmwareFileName stringValue];
    NSURL *url = [NSURL URLWithString:fileName];
    bool b = [url checkResourceIsReachableAndReturnError:&error];
    if( b == false )
    {
        //return;
    }
    NSData *digest = [self sha256:url];
    [_sonoffAPI set_ota_flash:d fileName:fileName digest:digest];

}

- (IBAction)tableMenuGetInfoAction:(id)sender {
    Device *d = (Device*)[[_arrayController arrangedObjects] objectAtIndex:[_tableView selectedRow]];
    [_sonoffAPI get_device_info:d];
}

- (IBAction)webServerControlAction:(id)sender {
}

- (IBAction)pulseWidthAction:(id)sender {
    Device *d = (Device*)[[_arrayController arrangedObjects] objectAtIndex:[_tableView selectedRow]];
    NSString *string = [_pulseWidthTextField stringValue];
    bool b = [sender state];
    NSNumber *pulseWidth = [NSNumber numberWithInt:[string intValue]];
    [_sonoffAPI set_pulse_width:d state:b pulseWidth:pulseWidth];
}

- (IBAction)wifiScanAction:(id)sender {
    CWWiFiClient *wfc = CWWiFiClient.sharedWiFiClient;
    CWInterface *wif = wfc.interface;
    DDLogInfo(@"Default wifi interface name: %@", wif.interfaceName);

    CWPHYMode phyMode = wif.activePHYMode;
    DDLogInfo(@"Phy mode = %ld\n", (long)phyMode );
    DDLogInfo(@"Intf. mode: %ld\n", (long)wif.interfaceMode);
    
    NSError *err;
    NSSet *scanset = [wif scanForNetworksWithSSID:Nil includeHidden:TRUE error:&err];
    if (err) {
        DDLogInfo(@"Scan failed, err=%ld\n", err.code);
        return;
    }
    DDLogInfo(@"Scan found %d networks", (int)scanset.count);
    NSMutableArray *array = [[NSMutableArray alloc] init];
    [scanset enumerateObjectsUsingBlock:^(id  _Nonnull obj, BOOL * _Nonnull stop) {
        CWNetwork *cwn = obj;
        NSDictionary *dict = [cwn valueForKey:@"_scanRecord"];
        NSString *ssid_str = [dict valueForKey:@"SSID_STR"];
        if (ssid_str != nil)
        {
            long signal = [[dict valueForKey:@"RSSI"] longValue];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setValue:ssid_str forKey:@"ssid"];
            [dict setValue:[NSNumber numberWithLong:signal] forKey:@"signal"];
            [array addObject:dict];
        }
    }];
    
    NSStoryboard *storyBoard = [NSStoryboard mainStoryboard];
    WifiNetworksViewController *wifiNetworksViewController = [storyBoard instantiateControllerWithIdentifier:@"wifiNetworksView"];
    [wifiNetworksViewController setWifiNetworks:array];
    NSPopover *popOver = [[NSPopover alloc] init];
    [popOver setBehavior:NSPopoverBehaviorTransient];
    [popOver setAnimates:YES];
    [popOver setContentViewController:wifiNetworksViewController];
    [popOver setBehavior:NSPopoverBehaviorTransient];
    NSRect entryRect = [sender convertRect:[sender bounds] toView:[[NSApp mainWindow] contentView] ];
    [popOver showRelativeToRect:entryRect ofView:[[NSApp mainWindow] contentView] preferredEdge:NSMinYEdge];

}

#
# pragma Observers
#
-(void)somethingHappens:(NSNotification*)notification
{
    NSString *string = [notification object];
    [_ssidTextField setStringValue:string];
}

-(void)updateDownloadProgressBar:(NSNotification*)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSNumber *number = [notification object];
        double v = [self->_downloadProgressBar doubleValue];
        v += [number doubleValue];
        [self->_downloadProgressBar setDoubleValue:v];
    });
}

#pragma mark NSNetServiceBrowserDelegate Delegate Methods
// Tells the delegate the sender found a domain.
- (void)netServiceBrowser:(NSNetServiceBrowser *)browser
            didFindDomain:(NSString *)domainString
               moreComing:(BOOL)moreComing
{

}

// Tells the delegate the a domain has disappeared or has become unavailable.
- (void)netServiceBrowser:(NSNetServiceBrowser *)browser
          didRemoveDomain:(NSString *)domainString
               moreComing:(BOOL)moreComing
{

}

// Tells the delegate the sender found a service.
- (void)netServiceBrowser:(NSNetServiceBrowser *)browser
           didFindService:(NSNetService *)service
               moreComing:(BOOL)moreComing
{
    DDLogInfo(@"Service Discovered");
    DDLogInfo(@"-   name: %@", [service name]);
    DDLogInfo(@"-   type: %@", [service type]);
    DDLogInfo(@"- domain: %@", [service domain]);
    
    if ([_services containsObject:service] == false)
    {
        Device *d = [[Device alloc] init];
        [d setNetService:service];
        [d setName:[service name]];
        [_arrayController addObject:d];
        
        [_services addObject:service];
        [service scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [service setDelegate:self];
        [service resolveWithTimeout:5];
        [service startMonitoring];
    }
    
    if (!moreComing)
    {
        DDLogInfo(@"No more services comming");
    } else {
        DDLogInfo(@"More services comming");
    }
}

// Tells the delegate a service has disappeared or has become unavailable.
- (void)netServiceBrowser:(NSNetServiceBrowser *)browser
         didRemoveService:(NSNetService *)service
               moreComing:(BOOL)moreComing
{

}

// Tells the delegate that a search is commencing.
- (void)netServiceBrowserWillSearch:(NSNetServiceBrowser *)browser
{
    DDLogInfo(@"NetSerivce browser begining");
}

// Tells the delegate that a search was not successful.
- (void)netServiceBrowser:(NSNetServiceBrowser *)browser
             didNotSearch:(NSDictionary<NSString *,NSNumber *> *)errorDict
{
}

// Tells the delegate that a search was stopped.
- (void)netServiceBrowserDidStopSearch:(NSNetServiceBrowser *)browser
{

}

#pragma mark NSNetServiceDelegate Delegate Methods
// Notifies the delegate that the network is ready to publish the service.
- (void)netServiceWillPublish:(NSNetService *)sender
{

}

//Notifies the delegate that a service could not be published.
- (void)netService:(NSNetService *)sender
     didNotPublish:(NSDictionary<NSString *,NSNumber *> *)errorDict
{
}

//Notifies the delegate that a service was successfully published.
- (void)netServiceDidPublish:(NSNetService *)sender
{
}

// Notifies the delegate that the network is ready to resolve the service.
- (void)netServiceWillResolve:(NSNetService *)sender
{
    DDLogInfo(@"NetSerivce attempting to resolve services");
}

// Informs the delegate that an error occurred during resolution of a given service.
- (void)netService:(NSNetService *)sender
     didNotResolve:(NSDictionary<NSString *,NSNumber *> *)errorDict
{

}

// Informs the delegate that the address for a given service was resolved.
- (void)netServiceDidResolveAddress:(NSNetService *)sender
{
    DDLogInfo(@"Service resolved");

    Device *device = [self arrayControllerGetDeviceForNetService:sender];
    
    NSString *serviceIP = [self resolveIPv4:[sender addresses]];
    if (serviceIP)
    {
        DDLogInfo(@"Found IPv4: %@", serviceIP);
        [device setIp_address:serviceIP];
    } else
    {
        DDLogInfo(@"Did not find IPv4 address");
    }
    
    NSData *txtRecordData = [sender TXTRecordData];
    if (txtRecordData)
    {
        NSDictionary *txtRecordDataDictionary = [self txtRecordDataToDictionary:txtRecordData];
        [self updateDeviceFromTXTRecordData:device txtRecordDataDictionary:txtRecordDataDictionary];
    }
    
    [sender stop];
    [_sonoffAPI get_device_info:device];
}

// Notifies the delegate that the TXT record for a given service has been updated.
- (void)netService:(NSNetService *)sender
    didUpdateTXTRecordData:(NSData *)data
{
    DDLogInfo(@"Service updated");
    NSString *serviceIP = [self resolveIPv4:[sender addresses]];
    if (serviceIP)
    {
        DDLogInfo(@"Found IPv4: %@", serviceIP);
    } else
    {
        DDLogInfo(@"Did not find IPv4 address");
    }
    
    NSData *txtRecordData = [sender TXTRecordData];
    if (txtRecordData)
    {
        NSDictionary *txtRecordDataDictionary = [self txtRecordDataToDictionary:txtRecordData];
        [self updateArrayControllerDataForService:sender dictionary:txtRecordDataDictionary IPv4:serviceIP];
        DDLogInfo(@"Received Data\n%@", txtRecordDataDictionary);
    }

}

// Informs the delegate that a publish or resolveWithTimeout: request was stopped.
- (void)netServiceDidStop:(NSNetService *)sender
{
}

#
#pragma Helper Functions
#
-(NSString *)resolveIPv4:(NSArray *)array
{
    DDLogInfo(@"Getting IPv4 addresses");
    NSMutableString *s = [[NSMutableString alloc] init];
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSData *data = (NSData *)obj;
        struct sockaddr_in address;
        int structSize = sizeof(struct sockaddr_in);
        [data getBytes:&address length:structSize];
        if (address.sin_family == AF_INET && s.length==0)
        {
            [s appendString:[NSString stringWithFormat:@"%s", inet_ntoa(address.sin_addr)]];
        }
    }];

    return s;
}

-(NSDictionary *)txtRecordDataToDictionary:(NSData*)txtRecordData
{
    NSError *error;
    NSMutableDictionary *txtRecordDataDictionary = (NSMutableDictionary*)[NSNetService dictionaryFromTXTRecordData:txtRecordData];
    NSData *data1 = [txtRecordDataDictionary valueForKey:@"data1"];
    NSDictionary *tempDictionary = [NSJSONSerialization JSONObjectWithData:data1 options:kNilOptions error:&error];
    
    [txtRecordDataDictionary removeObjectForKey:@"data1"];
    [txtRecordDataDictionary setObject:tempDictionary forKey:@"data1"];
    
    return txtRecordDataDictionary;
}

-(Device *)arrayControllerGetDeviceForNetService:(NSNetService *)service
{
    __block Device *device = nil;
    [[_arrayController arrangedObjects] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        Device *d = obj;
        if ( [d netService] == service)
        {
            if ( [d netService] == service)
            {
                 device = d;
                *stop = TRUE;
            }
        }
    }];
    return  device;
}

-(void)updateDeviceFromTXTRecordData:(Device *)device txtRecordDataDictionary:(NSDictionary *)txtRecordDataDictionary
{
    NSDictionary *data1 = [txtRecordDataDictionary objectForKey:@"data1"];
    
    [device setId:[[NSString alloc] initWithData:[txtRecordDataDictionary valueForKey:@"id"] encoding:NSUTF8StringEncoding]];
    [device setType:[[NSString alloc] initWithData:[txtRecordDataDictionary valueForKey:@"type"] encoding:NSUTF8StringEncoding]];
    
    [device setFw_version:[data1 valueForKey:@"fwVersion"]];
    [device setPulse_width:[[data1 valueForKey:@"pulseWidth"] longValue]];
    [device setSignal:[[data1 valueForKey:@"rssi"] longValue]];
    [device setCurrent_state:[[data1 objectForKey:@"switch"] isEqualTo:@"on"]];
    [device setPulse_state:[[data1 objectForKey:@"pulse"] isEqualTo:@"on"]];
    [device setStartupState:[data1 objectForKey:@"startup"]];
    [device setLed_state:[[data1 objectForKey:@"sledOnline"] isEqualTo:@"on"]];
}

-(void)updateArrayControllerDataForService:(NSNetService *)service dictionary:(NSDictionary *)txtRecordDataDictionary IPv4:(NSString *)IPv4
{
    [[_arrayController arrangedObjects] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        Device *d = obj;
        if ( [d netService] == service)
        {
            NSDictionary *data1 = [txtRecordDataDictionary objectForKey:@"data1"];
            
            [d setId:[[NSString alloc] initWithData:[txtRecordDataDictionary valueForKey:@"id"] encoding:NSUTF8StringEncoding]];
            [d setType:[[NSString alloc] initWithData:[txtRecordDataDictionary valueForKey:@"type"] encoding:NSUTF8StringEncoding]];
            
            [d setFw_version:[data1 valueForKey:@"fwVersion"]];
            [d setPulse_width:[[data1 valueForKey:@"pulseWidth"] longValue]];
            [d setSignal:[[data1 valueForKey:@"rssi"] longValue]];
            [d setCurrent_state:[[data1 objectForKey:@"switch"] isEqualTo:@"on"]];
            [d setPulse_state:[[data1 objectForKey:@"pulse"] isEqualTo:@"on"]];
            [d setStartupState:[data1 objectForKey:@"startup"]];
            [d setLed_state:[[data1 objectForKey:@"sledOnline"] isEqualTo:@"on"]];
            
            if (IPv4)
            {
                [d setIp_address:IPv4];
            }
        }
    }];
}

- (NSString *)hexStringForData:(NSData *)data
{
    if (data == nil) {
        return nil;
    }
    
    NSMutableString *hexString = [NSMutableString string];
    
    const unsigned char *p = [data bytes];
    
    for (int i=0; i < [data length]; i++) {
        [hexString appendFormat:@"%02x", *p++];
    }
    
    return [hexString uppercaseString];
}

-(NSData *)sha256:(NSURL *)url
{
    DDLogInfo(@"Calculating hash for file %@",[url lastPathComponent]);
    NSError *error;
    uint64 offset = 0;
    uint32 chunkSize = 10240;
    
    CC_SHA256_CTX context;
    CC_SHA256_Init(&context);
    
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForReadingFromURL:url error:&error];
    NSData *data = [fileHandle readDataOfLength:chunkSize];
    while ([data length] > 0)
    {
        CC_SHA256_Update(&context, [data bytes], chunkSize);
        
        offset += [data length];
        [fileHandle seekToFileOffset:offset];
        data = [fileHandle readDataOfLength:chunkSize];
    }
    
    unsigned char md[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256_Final(md, &context);
    NSData *hashedData =[NSData dataWithBytes:md length:CC_SHA256_DIGEST_LENGTH];
 
    DDLogInfo(@"Calculated has %@",[self hexStringForData:hashedData]);
    return hashedData;
}

-(void)stoptWebServer
{
    if (_webServer != nil)
    {
        [_webServer stop];
        _webServer = nil;
        [_webServerStatusButton setImage:[NSImage imageNamed:@"NSStatusUnvailable"]];
    }
}

-(void)startWebServer:(NSURL *)firmwareFile
{
    if (_webServer == nil)
    {
        _webServer = [[GCDWebServer alloc] init];
    }


    NSString* filePath = [[self firmwareFileName] stringValue];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDownloadProgressBar:) name:@"updateProgressBar" object:nil];
    
    [_webServer addHandlerWithMatchBlock:^GCDWebServerRequest *(NSString* requestMethod, NSURL* requestURL, NSDictionary* requestHeaders, NSString* urlPath, NSDictionary* urlQuery) {
        if (![requestMethod isEqualToString:@"GET"]) {
                    return nil;
                }
                return [[GCDWebServerRequest alloc] initWithMethod:requestMethod url:requestURL headers:requestHeaders path:urlPath query:urlQuery];
    }  processBlock:^GCDWebServerResponse *(GCDWebServerRequest* request) {
        
        GCDWebServerResponse* response = nil;
             
        response = [GCDWebServerFileResponse responseWithFile:filePath byteRange:request.byteRange];
        [response setValue:@"bytes" forAdditionalHeader:@"Accept-Ranges"];
        
        if (response) {
            response.cacheControlMaxAge = 360;
        } else {
            response = [GCDWebServerResponse responseWithStatusCode:kGCDWebServerHTTPStatusCode_NotFound];
        }
        return response;
    }];

/*
    [_webServer addDefaultHandlerForMethod:@"GET"
                              requestClass:[GCDWebServerRequest class]
                              processBlock:^GCDWebServerResponse *(GCDWebServerRequest* request) {
        return [GCDWebServerDataResponse responseWithHTML:@"<html><body><p>Hello World</p></body></html>"];
    }];
*/
    
    [_webServerStatusButton setImage:[NSImage imageNamed:@"NSStatusAvailable"]];
    
    NSError *error;
    NSMutableDictionary* options = [NSMutableDictionary dictionary];
    [options setObject:[NSNumber numberWithInteger:42069] forKey:GCDWebServerOption_Port];
    [options setValue:@"Sonoff DIY Webserver" forKey:GCDWebServerOption_BonjourName];
    [options setObject:[WebServerConnection class] forKey:GCDWebServerOption_ConnectionClass];
    [_webServer startWithOptions:options error:&error];
    
    //[_webServer startWithPort:42069 bonjourName:@"Sonoff DIY Webserver"];
    DDLogInfo(@"Visit %@ in your web browser", _webServer.serverURL);
}

@end
