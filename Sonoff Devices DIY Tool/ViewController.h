//
//  ViewController.h
//  Sonoff Devices DIY Tool
//
//  Created by Ed The Head on 2020-09-27.
//

#import <Cocoa/Cocoa.h>
#import <CommonCrypto/CommonHMAC.h>
#import <GCDWebServers/GCDWebServers.h>

#import "Device.h"
#import "SonoffAPI.h"
#import "WifiNetworksViewController.h"
#import "WebServerConnection.h"

@interface ViewController : NSViewController<NSNetServiceBrowserDelegate,NSNetServiceDelegate>

@property (strong) SonoffAPI *sonoffAPI;

@property (strong) IBOutlet NSTableView *tableView;
@property (strong) IBOutlet NSArrayController *arrayController;
@property (weak) IBOutlet NSButton *firmwareFileChooser;
@property (strong) IBOutlet NSTextField *firmwareFileName;
@property (strong) IBOutlet NSSwitch *ledStateSwitch;
@property (strong) IBOutlet NSSwitch *powerStateSwitch;
@property (strong) IBOutlet NSTextField *ssidTextField;
@property (strong) IBOutlet NSTextField *pulseWidthTextField;
@property (strong) IBOutlet NSButton *webServerStatusButton;
@property (strong) IBOutlet NSButton *webServerCheckBox;
@property (strong) IBOutlet NSTextField *webServerAddress;
@property (strong) IBOutlet NSButton *flashButton;
@property (unsafe_unretained) IBOutlet NSTextView *logTextView;
@property (strong) IBOutlet NSProgressIndicator *downloadProgressBar;

@property (strong) NSFont *font;
@property (strong) NSNetServiceBrowser *netServiceBrowser;
@property (strong,atomic) NSMutableArray *services;
@property (strong) NSImage *imageStatusUnavailable;
@property (strong) GCDWebServer *webServer;

@property BOOL isSearching;


- (IBAction)currentStateAction:(id)sender;
- (IBAction)startupStateAction:(id)sender;
- (IBAction)firmwareFileChooserAction:(id)sender;
- (IBAction)flashButtonAction:(id)sender;
- (IBAction)tableMenuGetInfoAction:(id)sender;
- (IBAction)wifiScanAction:(id)sender;
- (IBAction)pulseWidthAction:(id)sender;
- (IBAction)webServerControlAction:(id)sender;

@end

