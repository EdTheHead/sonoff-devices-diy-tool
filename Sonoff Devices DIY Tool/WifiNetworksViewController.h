//
//  WifiNetworksViewController.h
//  Sonoff Devices DIY Tool
//
//  Created by Ed The Head on 2020-10-05.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface WifiNetworksViewController : NSViewController<NSTableViewDelegate, NSTableViewDataSource>

@property (nonatomic) NSArray *wifiNetworks;
@property (strong) IBOutlet NSTableView *tableView;


@end

NS_ASSUME_NONNULL_END
