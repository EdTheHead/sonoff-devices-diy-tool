//
//  Device.h
//  Sonoff Devices DIY Tool
//
//  Created by Ed The Head on 2020-09-26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Device : NSObject

typedef NS_ENUM(NSInteger, StartupStates)
{
  OFF,
  ON,
  STAY
};

@property NSNetService *netService;
@property NSString *name;
@property NSString *id;
@property NSString *type;
@property NSString *ip_address;
@property NSString *ssid;
@property NSString *ssid_password;
@property NSString *bssid;
@property NSString *port;
@property NSString *fw_version;
@property long pulse_width;
@property long signal;

@property Boolean current_state;

@property Boolean startup_state_on;
@property Boolean startup_state_off;
@property Boolean startup_state_stay;

@property Boolean pulse_state;
@property Boolean led_state;
@property Boolean ota_unlocked;

-(void) setStartupState:(NSString *)startup_state;

//
// Debug Stuff
//
@property NSNumber *seq;

@end

NS_ASSUME_NONNULL_END
