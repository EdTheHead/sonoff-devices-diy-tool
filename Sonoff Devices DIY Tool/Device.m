//
//  Device.m
//  Sonoff Devices DIY Tool
//
//  Created by Ed The Head on 2020-09-26.
//

#import "Device.h"

@implementation Device

- (instancetype)init
{
    self = [super init];
    if (self) {
        _port = @"8081";
        _current_state = false;
        _startup_state_off = false;
        _startup_state_on = false;
        _startup_state_stay = false;
        _led_state = false;
        _ota_unlocked = false;
        _seq = [NSNumber numberWithLong:0];

    }
    return self;
}

-(void) setStartupState:(NSString *)startup_state
{
    if ([startup_state isEqualToString:@"off"])
    {
        _startup_state_off = true;
        _startup_state_on = false;
        _startup_state_stay = false;
    } else if ([startup_state isEqualToString:@"on"])
    {
        _startup_state_off = false;
        _startup_state_on = true;
        _startup_state_stay = false;
    } else if ([startup_state isEqualToString:@"stay"])
    {
        _startup_state_off = false;
        _startup_state_on = false;
        _startup_state_stay = true;
    }
}
 

@end
