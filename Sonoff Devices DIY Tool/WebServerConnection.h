//
//  WebServerConnection.h
//  Sonoff Devices DIY Tool
//
//  Created by Ed De Sousa on 2020-10-07.
//

#import <Foundation/Foundation.h>
#import <GCDWebServers/GCDWebServers.h>

NS_ASSUME_NONNULL_BEGIN

@interface WebServerConnection : GCDWebServerConnection

@end

NS_ASSUME_NONNULL_END
