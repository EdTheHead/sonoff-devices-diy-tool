//
//  SonoffAPI.h
//  Sonoff Devices DIY Tool
//
//  Created by Ed The Head on 2020-09-28.
//

#import <Foundation/Foundation.h>
#import "Device.h"

NS_ASSUME_NONNULL_BEGIN

@interface SonoffAPI : NSObject<NSURLConnectionDelegate>

@property (strong)  NSMutableData *responseData;


-(void) set_wifi:(NSString *)ssid password:(NSString *)password;

-(void) get_signal;



-(void) get_device_info:(Device *)d;
-(void) set_switch_state:(Device *)d state:(BOOL)state;
-(void) set_powerup_state:(Device *)d state:(NSString *)state;
-(void) set_ota_unlock:(Device *)d state:(BOOL)state;
-(void) set_ota_flash:(Device *)d fileName:(NSString *)fileName digest:(NSData *)digest;
-(void) set_pulse_width:(Device *)d state:(BOOL)state pulseWidth:(NSNumber *)pulseWidth;
@end

NS_ASSUME_NONNULL_END
