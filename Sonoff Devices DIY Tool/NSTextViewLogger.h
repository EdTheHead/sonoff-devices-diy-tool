//
//  NSTextFieldLogger.h
//  Sonoff Devices DIY Tool
//
//  Created by Ed The Head on 2020-09-29.
//

#import <Foundation/Foundation.h>
#import <AppKit/NSTextView.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSTextViewLogger : NSObject <DDLogger,DDLogFormatter>

@property (nonatomic, weak) NSTextView *textView;

@property (nonatomic, assign) BOOL autoScrollsToBottom;

@property (nonatomic, strong) id <DDLogFormatter> logFormatter;

@property (nonatomic, strong) NSFont *font;

@end



NS_ASSUME_NONNULL_END
