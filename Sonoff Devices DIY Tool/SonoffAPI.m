//
//  SonoffAPI.m
//  Sonoff Devices DIY Tool
//
//  Created by Ed The Head on 2020-09-28.
//

#import "SonoffAPI.h"

@interface SonoffAPI()
-(NSString *)build_http_body:(Device *)device dictData:(NSDictionary *)dictData needSequence:(BOOL)needSequence;
@end

@implementation SonoffAPI

-(NSString *)build_http_body:(Device *)device dictData:(NSDictionary *)dictData needSequence:(BOOL)needSequence
{
    NSError *error = nil;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    [dict setValue:[device id] forKey:@"deviceid"];
    if (needSequence)
    {
        time_t unixTime = (time_t) [[NSDate date] timeIntervalSince1970];
        [dict setValue:[[NSNumber numberWithLong:unixTime] stringValue] forKey:@"sequence"];
    }
    [dict setValue:data forKey:@"data"];
    [data addEntriesFromDictionary:dictData];
    NSData *jsondata = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    NSString *s = [[NSString alloc] initWithData:jsondata encoding:NSUTF8StringEncoding];
    return s;
}

-(void)process_error:(int)errorNumber
{
    if (errorNumber==400) {
        DDLogDebug(@"The operation failed and the request was formatted incorrectly. The request body is not a valid JSON format.");
    } else
    if (errorNumber==401)
    {
        DDLogDebug(@"The operation failed and the request was unauthorized. Device information encryption is enabled on the device, but the request is not encrypted." );
    } else
    if (errorNumber==404)
    {
        DDLogDebug(@"The operation failed and the device does not exist. The device does not support the requested deviceid.");
    } else
    if (errorNumber==422)
    {
        DDLogDebug(@"The operation failed and the request parameters are invalid. For example, the device does not support setting specific device information.");
    } else
    if (errorNumber==500)
    {
        DDLogDebug(@"The operation failed and the device has errors. For example, the device ID or API Key error which is not authenticated by the vendor’s OTA unlock service.");
    } else
    if (errorNumber==503)
    {
        DDLogDebug(@"The operation failed and the device is not able to request the vendor’s OTA unlock service. For example, the device is not connected to WiFi, the device is not connected to the Internet, the manufacturer’s OTA unlock service is down, etc.");
    } else
    {
        DDLogDebug(@"Unsupported error number %d", errorNumber);
    }
}

-(void) get_device_info:(Device *)d
{
    NSString *api_url = [NSString stringWithFormat:@"http://%@:%@/%@", [d ip_address], [d port], @"zeroconf/info"];
    DDLogDebug(@"URL: %@", api_url);
    NSURL *url = [NSURL URLWithString:api_url];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[[self build_http_body:d dictData:nil needSequence:FALSE] dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                             completionHandler:
         ^(NSData *data, NSURLResponse *response, NSError *error) {
            if(error)
            {
                DDLogDebug(@"Get Device Info Response Error");
            } else {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
                
                NSDictionary *dict_data = [dict valueForKey:@"data"];
                if ([[dict valueForKey:@"error"] integerValue] == 0 )
                {
                    [d setCurrent_state:[[dict_data objectForKey:@"switch"] isEqualTo:@"on"]];
                    [d setFw_version:[dict_data valueForKey:@"fwVersion"]];
                    [d setId:[dict_data valueForKey:@"deviceid"]];
                    [d setSignal:[[dict_data valueForKey:@"signalStrength"] longValue]];
                    [d setPulse_state:[[dict_data objectForKey:@"pulse"] isEqualTo:@"on"]];
                    [d setBssid:[dict_data valueForKey:@"bssid"]];
                    [d setSsid:[dict_data valueForKey:@"ssid"]];
                    [d setPulse_width:[[dict_data valueForKey:@"pulseWidth"] longValue]];
                    [d setOta_unlocked:[[dict_data valueForKey:@"otaUnlock"] boolValue]];
                    [d setStartupState:[dict_data valueForKey:@"startup"]];
                } else
                {
                    [self process_error:(int)[[dict valueForKey:@"error"] integerValue]];
                }
            }
         }];
    [task resume];
}

-(void) set_switch_state:(Device *)d  state:(BOOL)state;
{
    NSString *api_url = [NSString stringWithFormat:@"http://%@:%@/%@", [d ip_address], [d port], @"zeroconf/switch"];
    DDLogDebug(@"URL: %@", api_url);
   
    NSURL *url = [NSURL URLWithString:api_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url ];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:[d current_state] ? @"on" : @"off" forKey:@"switch"];
    [request setHTTPBody:[[self build_http_body:d dictData:dict needSequence:TRUE] dataUsingEncoding:NSUTF8StringEncoding]];

    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                             completionHandler:
         ^(NSData *data, NSURLResponse *response, NSError *error) {
            if(error)
            {
                DDLogDebug(@"Set Switch State Response Error");
            } else {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
                DDLogDebug(@"Response: %@", dict);
                if ([[dict valueForKey:@"error"] integerValue] != 0 )
                {
                    [self process_error:(int)[[dict valueForKey:@"error"] integerValue]];
                }
            }
            
         }];
    [task resume];
}

-(void) set_powerup_state:(Device *)d state:(NSString *)state
{
    NSString *api_url = [NSString stringWithFormat:@"http://%@:%@/%@", [d ip_address], [d port], @"zeroconf/startup"];
    DDLogDebug(@"URL: %@", api_url);
    
    NSURL *url = [NSURL URLWithString:api_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url ];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:state forKey:@"startup"];
    [request setHTTPBody:[[self build_http_body:d dictData:dict needSequence:TRUE] dataUsingEncoding:NSUTF8StringEncoding]];

    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                             completionHandler:
         ^(NSData *data, NSURLResponse *response, NSError *error) {
            if(error)
            {
                DDLogDebug(@"Set Powerup State Response Error");
            } else {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
                DDLogDebug(@"Response: %@", dict);
                if ([[dict valueForKey:@"error"] integerValue] != 0 )
                {
                    [self process_error:(int)[[dict valueForKey:@"error"] integerValue]];
                }
            }
            
         }];
    [task resume];
}

-(void) set_ota_unlock:(Device *)d  state:(BOOL)state;
{
    NSString *api_url = [NSString stringWithFormat:@"http://%@:%@/%@", [d ip_address], [d port], @"zeroconf/ota_unlock"];
    DDLogDebug(@"URL: %@", api_url);
    
    NSURL *url = [NSURL URLWithString:api_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url ];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPBody:[[self build_http_body:d dictData:@{} needSequence:TRUE] dataUsingEncoding:NSUTF8StringEncoding]];

    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                             completionHandler:
         ^(NSData *data, NSURLResponse *response, NSError *error) {
            if(error)
            {
                DDLogDebug(@"Set Powerup State Response Error");
            } else {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
                DDLogDebug(@"Response: %@", dict);
                if ([[dict valueForKey:@"error"] integerValue] != 0 )
                {
                    [self process_error:(int)[[dict valueForKey:@"error"] integerValue]];
                }
            }
            
         }];
    [task resume];
}

-(void) set_ota_flash:(Device *)d fileName:(NSString *)fileName digest:(NSData *)digest
{
    NSString *api_url = [NSString stringWithFormat:@"http://%@:%@/%@", [d ip_address], [d port], @"zeroconf/ota_flash"];
    DDLogDebug(@"URL: %@", api_url);
    
    NSURL *url = [NSURL URLWithString:api_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url ];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:@"downloadUrl" forKey:@"downloadUrl"];
    [dict setValue:@"sha256" forKey:@"sha256sum"];
    [request setHTTPBody:[[self build_http_body:d dictData:dict needSequence:TRUE] dataUsingEncoding:NSUTF8StringEncoding]];

    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                             completionHandler:
         ^(NSData *data, NSURLResponse *response, NSError *error) {
            if(error)
            {
            } else {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
                DDLogDebug(@"Response: %@", dict);
            }
            
         }];
    [task resume];
}



-(void) set_wifi:(NSString *)ssid password:(NSString *)password
{
    
}

-(void) set_pulse_width:(Device *)d state:(BOOL)state pulseWidth:(NSNumber *)pulseWidth;
{
    NSString *api_url = [NSString stringWithFormat:@"http://%@:%@/%@", [d ip_address], [d port], @"zeroconf/pulse"];
    DDLogDebug(@"URL: %@", api_url);
    
    NSURL *url = [NSURL URLWithString:api_url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url ];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:pulseWidth forKey:@"pulsewidth"];
    [dict setValue:[NSNumber numberWithBool:state] forKey:@"pulse"];
    [request setHTTPBody:[[self build_http_body:d dictData:dict needSequence:TRUE] dataUsingEncoding:NSUTF8StringEncoding]];

    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                             completionHandler:
         ^(NSData *data, NSURLResponse *response, NSError *error) {
            if(error)
            {
            } else {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
                DDLogDebug(@"Response: %@", dict);
            }
            
         }];
    [task resume];
}

-(void) get_signal
{
    
}
@end
