//
//  AppDelegate.m
//  Sonoff Devices DIY Tool
//
//  Created by Ed The Head on 2020-09-27.
//

#import "AppDelegate.h"

@interface AppDelegate ()


@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    [DDLog addLogger:[DDOSLogger sharedInstance]];
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
