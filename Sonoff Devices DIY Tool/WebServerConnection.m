//
//  WebServerConnection.m
//  Sonoff Devices DIY Tool
//
//  Created by Ed De Sousa on 2020-10-07.
//

#import "WebServerConnection.h"

@implementation WebServerConnection

- (void)didWriteBytes:(const void*)bytes length:(NSUInteger)length {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateProgressBar" object:[NSNumber numberWithLong:length]];
    [super didWriteBytes:bytes length:length];
}

@end
