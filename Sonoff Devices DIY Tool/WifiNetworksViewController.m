//
//  WifiNetworksViewController.m
//  Sonoff Devices DIY Tool
//
//  Created by Ed The Head on 2020-10-05.
//

#import "WifiNetworksViewController.h"

@interface WifiNetworksViewController ()

@end

@implementation WifiNetworksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
}

-(void)setWifiNetworks:(NSArray *)wifiNetworks
{
    _wifiNetworks = wifiNetworks;
}

- (IBAction)tableViewDoubleClickAction:(id)sender {
    NSString *string = [[_wifiNetworks objectAtIndex:[_tableView selectedRow]] objectForKey:@"ssid"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationName" object:string];
}


#pragma  Tableview Delegate Methods
- (NSView *)tableView:(NSTableView *)tableView
   viewForTableColumn:(NSTableColumn *)tableColumn
                  row:(NSInteger)row
{
    if ([_wifiNetworks count] == 0)
    {
        return nil;
    }
    
    NSTableCellView *cell = [tableView makeViewWithIdentifier:[tableColumn identifier] owner:nil];
    [[cell textField] setStringValue:[[_wifiNetworks objectAtIndex:row] objectForKey:[tableColumn identifier]]];
    return cell;
}


#pragma  Tableview Datasource Methods

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    return [_wifiNetworks count];
}


@end
